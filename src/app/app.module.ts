import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import {NgxPaginationModule} from 'ngx-pagination';


import { AppComponent } from './components/app/app.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ErrorComponent } from './components/error/error.component';
import { UsersComponent } from './components/users/users.component';
import { SearchRepositoriesComponent } from './components/searchRepositories/searchRepositories.component';
import { SignupComponent } from './components/signup/signup.component';
import { ChatComponent } from './components/chat/chat.component';
import { QuizComponent } from './components/quiz/quiz.component';

import { Users } from './services/users.service';
import { SignupService } from './services/signup.service';
import { SearchRepositoriesService } from './services/search-repositories.service';
import { ChatService } from './services/chat.service';
import { QuizService } from './services/quiz.service';


const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'about', component: AboutComponent},
  {path: 'users', component: UsersComponent},
  {path: 'github', component: SearchRepositoriesComponent},
  {path: 'chat', component: ChatComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'my_profile', component: SignupComponent},
  {path: 'quiz', component: QuizComponent},
  {path: '**', component: ErrorComponent},
];

@NgModule({
  declarations: [
    AppComponent, HomeComponent, AboutComponent, ErrorComponent, UsersComponent, SearchRepositoriesComponent, SignupComponent,
      ChatComponent, QuizComponent
  ],
  imports: [
    BrowserModule, RouterModule.forRoot(appRoutes), FormsModule, ReactiveFormsModule, BrowserAnimationsModule, HttpModule, NgxPaginationModule
  ],
  providers: [Users, SearchRepositoriesService, SignupService, ChatService, QuizService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, Input } from "@angular/core";
import { Users } from "../../services/users.service";
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { trigger, state, style, transition, animate, keyframes, query, stagger} from "@angular/animations";
import * as suService from "../../services/signup.service";

@Component({
    selector: "users-form",
    templateUrl: "users.component.html",
    styleUrls: ["users.component.css"],
    animations: [
        trigger("inputState", [
            state("1", style({
                opacity:1,
                transform: 'translateX(0%)'
            })),
            state("0", style({
                transform: 'translateX(-110%)',
                opacity:0
            }))
        ]),
        trigger("addUserForm", [
            state("1", style({
                display:'block'
            })),
            state("0", style({
                display:'none'
            })),
            transition("1 => 0", [
                query('.anim', stagger('150ms', [
                        animate("0.5s ease-in", keyframes([
                            style({ transform: 'translateX(0%)', opacity: 1,  offset: 0}),
                            style({ transform: 'translateX(100px)', opacity:0.5,  offset: 0.3}),
                            style({ transform: 'translateX(-110%)', opacity:0, offset: 1.0})
                        ]))
                    ]), {optional: true}
                )]),
            transition("0 => 1", [
                query('.anim', stagger('150ms', [
                    animate("0.5s ease-in", keyframes([
                        style({ transform: 'translateX(-110%)', opacity:0,  offset: 0}),
                        style({ transform: 'translateX(100px)', opacity:0.5, offset: 0.3}),
                        style({ transform: 'translateX(0%)', opacity:1, offset: 1.0})
            ]))
                    ]), {optional: true}
                )
            ])
        ]),
        trigger("removeUser", [
                transition('* => *', [

                    query(':enter', stagger('300ms', [
                        animate('1s ease-in', keyframes([
                            style({opacity: 0, transform: 'translateX(-100%)', offset: 0}),
                            style({opacity: .5, transform: 'translateX(35px)',  offset: 0.3}),
                            style({opacity: 1, transform: 'translateX(0)',     offset: 1.0}),
                        ]))]), {optional: true}),
                    query(':leave', stagger('300ms', [
                        animate('1s ease-in', keyframes([
                            style({opacity: 1, transform: 'translateX(0)', offset: 0}),
                            style({opacity: .5, transform: 'translateX(35px)',  offset: 0.3}),
                            style({opacity: 0, transform: 'translateX(100%)',     offset: 1.0}),
                        ]))]), {optional: true})
                ])
            ])
        ]

})
export class UsersComponent implements OnInit{
    users;
    userName:string;
    userAge:number;
    email:string;
    editEmail:string;
    password:string;
    cpassword:string;
    emailMatch:boolean;
    removeState:string = "";
    inputState:number = 0;
    submitButton:boolean = true;
    editIndex:number;
    usersForm:FormGroup;
    inpSt:number = 0;
    @Input() aboutPage:boolean = true;


    constructor(private usersData:Users){}

    ngOnInit() {
        this.users = this.usersData.getUsers();
        this.usersForm = new FormGroup({
            name: new FormControl("", Validators.required),
            userEmail: new FormControl("", [Validators.required, Validators.email, this.matchEmail.bind(this)]),
            age: new FormControl("", [Validators.required, Validators.min(1), Validators.max(100)]),
            userPassword: new FormControl("", Validators.required),
            confirmPassword: new FormControl("", [Validators.required, this.matchPass.bind(this)])
        });
    }
    matchEmail(control){
        if(this.users.find(u => u.email === control.value) && control.value != this.editEmail) {
            this.emailMatch = true;
            return {"userEmail": true};
        } else {
            this.emailMatch = false;
            return null;
        }
    }

    matchPass(control){
        if(this.password !== control.value) {
            return {"confirmPassword": true};
        } else {
            return null;
        }
    }
    addUser() {
        this.usersData.addUserData(this.userName, this.email, this.password, this.userAge);
    }

    animateForm(form='submit') {
        if (form=='edit') {
            this.inputState =  1;
        } else{
            if(this.submitButton){
                this.inputState = this.inputState == 0 ? 1 : 0;
            } else {
                this.inputState = 1;
                this.showUserData(this.editIndex);
            }   
        }
    }

    done(ev){
        if(ev.toState != 0){
            this.inpSt = 1;
        }
    }
    start(ev){
        if(ev.toState != 0){
            this.inpSt = 0;
        }
    }
    removeUser(i){
        console.log(event);
        this.removeState = 'remove';
        this.usersData.deleteUser(i);
    }
    showUserData(i){
        if(this.editIndex == i) {
            this.submitButton = !this.submitButton;
            this.userName = this.email = this.cpassword = this.password = this.editEmail = "";
            this.userAge = this.editIndex = undefined;
        } else {
            this.submitButton = false;
            this.editIndex = i;
            this.userName = this.users[i]['name'];
            this.email = this.users[i]['email'];
            this.userAge = this.users[i]['age'];
            this.password = this.users[i]['password'];
            this.cpassword = this.users[i]['password'];
            this.editEmail = this.users[i]['email'];
        }
    }
    editUser() {
        this.usersData.edit(this.editIndex, this.userName, this.email, this.password, this.userAge);
    }

}
import { Component } from '@angular/core';
import { QuizService} from '../../services/quiz.service';
import {animate, transition, keyframes, style, trigger} from '@angular/animations';


@Component({
    selector: 'app-skills-test',
    templateUrl: 'quiz.component.html',
    styleUrls: ['quiz.component.css'],
    animations: [
        trigger('next', [
            transition('*=>*',
                animate('0.1s', keyframes([
                    style({ opacity: 0, offset: 0}),
                    style({ opacity: 0.6, offset: 0.8}),
                    style({ opacity: 1,  offset: 1.0})
                ])))
        ])
    ]
})
export class QuizComponent {
    questions;
    answers = [];
    qLength: number;
    num = 1;
    answered = false;
    score = 0;
    complated;
    skipped = 0;
    state = 0;
    time = 30;
    constructor(private quiz: QuizService) {
        this.quiz.getTimeOut();
    }
    getTimeOut() {
        const interval = setInterval(() => {
            this.time--;
            if(this.complated){
                clearInterval(interval);
            }
            if (this.time == 0) {
                clearInterval(interval);
                this.complated = true;
                this.quiz.updateNum(0);
            }
        }, 1000);

    }
    start() {
        this.getTimeOut();
        this.quiz.getQuestions();
        this.quiz.quiz$.subscribe(v => {
            this.quiz.getQuestionsLength().then(snapshot => {
                this.qLength = Object.keys(snapshot.val()).length;
                this.state++;
                this.questions = v;
                const arr = Object.keys(this.questions.answers);
                let i = 0;
                this.answers.length = 0;
                for (const key of arr) {
                    this.answers[i] = this.questions.answers[key];
                    i++;
                }
            }).catch(e => console.log(e));

        });
    }
    check(evn, answ) {
        const correct = this.questions.correct;
        this.answered = true;
        if (correct == answ) {
            evn.target.className += ' btn-success';
            this.score++;
        } else {
            evn.target.className += ' btn-danger';
            const correctBtn = document.querySelector('[data-answer=\'' + correct + '\']');
            correctBtn.className += ' btn-success';
            console.log(correctBtn);
        }
    }
    continue(skip = false) {
        if (this.num !== this.qLength) {
            this.num += 1;
            this.quiz.getQuestions();
            this.answered = false;
        } else {
            this.complated = true;
            this.quiz.updateNum(0);
        }
        if (skip === true) {
            this.skipped++;
        }
    }
}
/*                Array.prototype.forEach.call(snapshot.val(), (function (element, index, array) {
                    console.log(element);
                    console.log(index);
                    console.log(array);
                }));*/
/*
 const lights = document.querySelectorAll('.ast');
[].forEach.call(lights, function(el) {
                console.log(el);
                el.classList.remove('btn-danger');
                el.classList.remove('btn-success');
            });*/

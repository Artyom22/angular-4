import { Component } from '@angular/core';
import * as suService from '../../services/signup.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {
  logedUserId;
  userData;
  start: boolean = this.sign.start;
  constructor(private sign: suService.SignupService, private router: Router) {
    this.start = this.sign.start;
    this.sign.checkUser().then(v => {
      this.sign.takeUserData(v[0]);
      this.sign.checkEmailVerification(v[1]);
    }).catch(() => { this.sign.addStreamData({id: null, data: null, start: true}); });

    this.sign.data$.subscribe(v => {
      this.logedUserId = v.id;
      this.userData = v.data;
      this.start = v.start;
    });

  }
  signOut = () => {
    this.sign.signOutUser();
    this.router.navigateByUrl('/signup');
  }
}

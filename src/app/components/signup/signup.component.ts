import {Component, Input, OnInit, Renderer2, ViewChild} from '@angular/core';
// import { NgForm } from "@angular/forms";
import {animate, state, query, transition, keyframes, stagger, style, trigger} from '@angular/animations';
import * as suService from '../../services/signup.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
    selector: 'sign-up',
    templateUrl: 'signup.component.html',
    styleUrls: ['signup.component.css'],
    animations: [
        trigger('hideSignIn', [
            state('0', style({display: 'block'})),
            state('1', style({display: 'none'})),
            transition('0=>1',
                animate('0.5s', keyframes([
                style({ transform: 'translateX(-100%) rotate(360deg) scale(1)', offset: 0}),
                style({ transform: 'translateX(0%) rotate(0deg) scale(0.6)', offset: 0.3}),
                style({ transform: 'translateX(300%) rotate(360deg) scale(0)', opacity: 0, offset: 1.0})
            ]))),
            transition('1=>0',
                animate('0.5s', keyframes([
                    style({ transform: 'translateX(300%) rotate(360deg) scale(0)', offset: 0}),
                    style({ transform: 'translateX(-100%) rotate(0deg) scale(0.6)', offset: 0.3}),
                    style({ transform: 'translateX(0%) rotate(360deg) scale(1)', opacity: 1, offset: 1.0})
            ])))
        ]),
        trigger('showSignUp', [
            transition('0=>1', [
                query('.anim', stagger('300ms', [
                    animate('0.7s 500ms', keyframes([
                        style({ transform: 'translateX(-800%) rotate(-720deg)', width: '5%', opacity: 0, offset: 0}),
                        style({ transform: 'translateX(50%)  rotate(360deg)', width: '10%', opacity: 0.4, offset: 0.7}),
                        style({ transform: 'translateX(0%)', width: '100%', opacity: 1, offset: 1.0})
                    ]))
                ]))
            ]
            )
        ]),
        trigger('forStagger', [
            state('0', style({opacity: 0, transform: 'translateX(-300%)',  width: '0%'})),
            state('1', style({opacity: 1,  width: '100%'}))
        ]),
        trigger('h3', [
            state('0', style({ opacity: 0,  transform: 'translateY(-800%)' })),
            state('1', style({ opacity: 1, transform: 'translateY(0%)' })),
            transition('0=>1', [
                //
            query(':self', style({opacity: 1})),
            query('h3', animate('0.5s',  keyframes([

                style({ transform: 'translateY(0%)', opacity: 0, offset: 0}),
                style({ transform: 'translateY(700%) translateX(50%)',  opacity: 1, offset: 0.7}),
                style({ transform: 'translateY(800%) translateX(0%)', offset: 1.0})
            ]))),
            query('button', animate('0.5s', keyframes([
                style({ transform: 'translateY(0%)', opacity: 0, offset: 0}),
                style({ transform: 'translateY(700%) translateX(-300px)',  opacity: 1, offset: 0.7}),
                style({ transform: 'translateY(800%) translateX(0%)', offset: 1.0})
            ])))
            ])
        ]),
        trigger('editForm', [
            transition('*=>1', [
                query('input', animate('0.2s', keyframes([
                    style({ width: '0', offset: 0}),
                    style({ width: '40%',  offset: 1})
                ]))),
                query('.edit', animate('0.2s', keyframes([
                    style({ transform: 'rotate(0deg)', offset: 0}),
                    style({ transform: 'rotate(360deg)',  offset: 1})
                ])))
            ]),
            transition('1=>0', [
                query('input', animate('0.05s', keyframes([
                    style({ width: '40%', offset: 0}),
                    style({ width: '0',  offset: 1})
                ])))
            ])
        ])
    ]
})
export class SignupComponent implements OnInit {
    email: string;
    password: string;
    confirmPassword: string;
    userData = this.sign.userData;
    logedUserId = this.sign.logedUserId;
    message;
    userName: string;
    age: number;
    signup = false;
    signinState = 0;
    signupState = 0;
    stagerState = 0;
    h3State = 0;
    start: boolean = this.sign.start;
    emailVerified: boolean = this.sign.emailVerified;
    sentEmail: boolean;
    showPass = false;
    editNameState: number;
    editAgeState: number;
    error = false;
    success = false;
    usersForm;
    userAvatar;
    signInEmail;
    signInPass;
    imgSrc;
    userImages = [];
    showImages = false;
    addNewImage = false;
    chngAva = false;
    setAvatar;

    @ViewChild('imageInput') imageInput;
    @ViewChild('multiImageInput') multiImageInput;

    constructor(private sign: suService.SignupService, private router: Router, private renderer: Renderer2) {/*
        this.start = this.sign.start;*//*
        this.signOut();*/
        this.sign.data$.subscribe(v => {
            this.logedUserId = v.id;
            this.userData = v.data;
            this.start = v.start;
            if (v.email !== undefined) {
                this.emailVerified = v.email;
            }
        });
    }

    ngOnInit() {
        this.usersForm = new FormGroup({
            name: new FormControl('', Validators.required),
            userEmail: new FormControl('', [Validators.required, Validators.email/*, this.matchEmail.bind(this)*/]),
            age: new FormControl('', [Validators.required, Validators.min(1), Validators.max(100)]),
            avatar: new FormControl(''),
            userPassword: new FormControl('', Validators.required),
            confirmPassword: new FormControl('', [Validators.required, this.matchPass.bind(this)])
        });
    }

    signIn = () => {
        this.sign.signIn(this.email, this.password).then(v => {
            this.sign.takeUserData(v.uid); this.sign.checkEmailVerification(v.emailVerified);
        }).catch(e => {this.sign.getUserData(null);
        this.showMessage('error'); this.message = e.message; });
        this.router.navigateByUrl('/my_profile');
    }
    matchPass(control) {
        if (this.password !== control.value) {
            return {'confirmPassword': true};
        } else {
            return null;
        }
    }
    signOut = () => {
        this.sign.signOutUser();
    }
    google = () => {
        this.sign.getGoogleUser().then(u => {
            this.sign.takeUserData(u.user.uid); this.logedUserId = u.user.uid; this.emailVerified = true;
        }).catch(() => {console.log('Something went wrong'); });
    }
    addUser = () => {this.sign.addUser(this.userName, this.email, this.age, this.password, this.imgSrc).then(v => {
        this.sign.takeUserData(v);
        this.sign.addImage(this.imgSrc);
        this.emailVerified = false;
    }).catch(e => {this.showMessage('error');  this.message = e; }); }

    verifyEmail = () => {suService.SignupService.verifyEmail(); this.sentEmail = true; };

    showPassForm() {
        this.showPass = !this.showPass;
    }

    changePassword = () => {
      if (this.password !== this.confirmPassword) {
          this.showMessage('error');
          this.message = 'Passwords aren\'t matching';
      } else {
          this.sign.updatePass(this.password).then(() => {
              this.showMessage('success');
              this.message =  'Your password updated successfully';
          }).catch(error => {
              this.showMessage('error');
              this.message =  error.message;
          });
      }
    }

    changeAvatar() {
        this.chngAva = true;
        console.log(this.chngAva);
    }
    setNewAvatar(link) {
        this.sign.updateAvatar(link);
    }

    updateName = () => {
        if (this.userName === undefined || this.userName === '') {
            this.showMessage('error');
            this.message = 'You can\'t add an empty name';
        } else if (this.userName === this.userData.name) {
            this.showMessage('error');
            this.message = 'You didn\'t change your name. If you didn\'t want to change your name please click on Cancel button';
        } else {
            this.sign.updateName(this.userName, this.logedUserId);
            this.showEditForm(1);
        }
    }
    showMessage(messageType) {
        if (messageType === 'error') {
            this.error = true;
            setTimeout(() => {this.error = false; }, 10000);
        } else {
            this.success = true;
            setTimeout(() => {this.success = false; }, 10000);

        }
    }
    updateAge = () => {
        if (this.age == null) {
            this.showMessage('error');
            this.message = 'You can\'t add an empty age';
        } else if (this.age === this.userData.name) {
            this.showMessage('error');
            this.message = 'You don\'t change your age. If you don\'t want to change your age please click on Cancel button';
        } else if (this.age < 0) {
            this.showMessage('error');
            this.message = 'Your age must be big of 0';
        } else {
            this.sign.updateAge(this.age, this.logedUserId);
            this.showEditForm(0);
        }
    }
    avatarCheck = (e) => {
        this.addNewImage = true;
        const fileBrowser = this.multiImageInput ? this.multiImageInput.nativeElement : this.imageInput.nativeElement;

        if (this.multiImageInput) {
            console.log('start');
            const parentDiv = document.querySelector('.loadedImages');
            let i;
            const loadedImages = document.getElementsByClassName('loadedImage');
            const anArray = loadedImages;
            if (anArray.length > 0) {
                // console.log(loadedImages);
                let k = 0;
                const len = anArray.length;
                while (k !== len) {
                    parentDiv.removeChild(anArray[0]);
                    k++;
                }
            }
            if (anArray.length === 0) {
                for (i = 0; i < fileBrowser.files.length; i++) {
                    const reader = new FileReader();
                    const name = fileBrowser.files[i].name;
                    console.log(name);
                    reader.onload = function(){
                        const output = <HTMLImageElement>document.createElement('img');
                        output.className = 'loadedImage';
                        output.dataset.name = name;
                        output.src = reader.result;
                        parentDiv.appendChild(output);
                    };
                    reader.readAsDataURL(fileBrowser.files[i]);
                }
            }
        } else {
            const reader = new FileReader();
            this.imgSrc = fileBrowser.files[0];
            reader.onload = function(){
                const output = <HTMLImageElement>document.getElementById('uploadImage');
                output.src = reader.result;
            };
            reader.readAsDataURL(this.imgSrc);
        }

        // this.imgSrc  = window.URL.createObjectURL(fileBrowser.files[0]);
/*
        console.log(fileBrowser.files);
        console.log(fileBrowser.files[0]);*/
    }

    addNewImages() {
        const fileBrowser = this.multiImageInput.nativeElement;
        const newImages: HTMLCollection = document.getElementsByClassName('loadedImage');
        const imgCont = document.querySelector('.images');
        let i = 0;
        let filesArr = [];
        for (let file in fileBrowser.files) {
            let key = fileBrowser.files[file].name;
            filesArr[key] = fileBrowser.files[file];
        }
        console.log(filesArr);
        for (let img in newImages) {
            console.log(img);
            if (img != 'length' && img != 'item' && img != 'namedItem') {
                console.log(img);
                const data = this.sign.addImage(filesArr[(newImages[img] as HTMLImageElement).dataset.name], true);
                let output = <HTMLImageElement>document.createElement('img');
                output.src = (newImages[img] as HTMLImageElement).src;
                this.renderer.listen(output, 'mouseover', evt => this.imgHover(evt, data));
                this.renderer.listen(output, 'mouseout', evt => this.imgOut(evt));
                this.renderer.listen(output, 'click', evt => this.setNewAvatar(data.link));
                let outputDiv = document.createElement('div');
                outputDiv.className = 'img';
                outputDiv.appendChild(output);
                imgCont.appendChild(outputDiv);
                i++;
            }
        }
        const parentDiv = document.querySelector('.loadedImages');
        const anArray = document.getElementsByClassName('loadedImage');
        if (anArray.length > 0) {
            console.log(anArray);
            let k = 0;
            const len = anArray.length;
            while (k !== len) {
                parentDiv.removeChild(anArray[0]);
                k++;
            }
        }

    }
    getUserImgs() {
        this.showImages = !this.showImages;
        this.sign.getUserImages().then(snapshot => {
            let imgs = snapshot.val();
            let keys = Object.keys(imgs);
            let i = 0;
            for (let key of keys) {
                this.userImages[i] = imgs[key];
                i++;
            }
            if (this.userImages == null) {
                this.message = 'You haven\'t images yet'; console.log(this.message);
            }
        }).catch(() => { this.message = 'You haven\'t images yet'; console.log(this.message);});
    }
    imgHover(img, link) {

        const hoveredImg = img.target;
        const checkRemoveBtn = document.querySelector('.removeImg');
        if (checkRemoveBtn === null) {

            const newText = document.createElement('div');
            newText.className = 'removeImg';
            newText.innerHTML = `<button class="btn btn-danger glyphicon glyphicon-remove"></button>`;
            hoveredImg.parentNode.insertBefore(newText, hoveredImg.nextSibling);
            if (this.chngAva === true) {
                const newElement = document.createElement('div');
                newElement.className = 'makeAvatar';
                newElement.innerText = 'Make avatar';
                hoveredImg.parentNode.insertBefore(newElement, hoveredImg.nextSibling);
            }
            const rem_btn = document.querySelector('.removeImg');
            this.renderer.listen(rem_btn, 'click', (evt) => {
                this.removeImg(evt, link);
                const parentImg = document.querySelector('.images');
                const removeImg = evt.target.parentNode.parentNode;
                parentImg.removeChild(removeImg);
            });
            this.renderer.listen(rem_btn, 'mouseout', (evt) => {
                const img = document.querySelector('.img img');
                // console.log(img.target);
                // console.log(btni);
                if (evt.relatedTarget !== img) {
                    const toImg = evt.fromElement.parentNode.parentNode;
                    const removeBtn = document.querySelector('.removeImg');
                    toImg.removeChild(removeBtn);
                }
            });
        }
    }
    imgOut(img) {
        const hoveredImg = img.target.parentNode;

        const btn = document.querySelector('.removeImg button');
        const btni = document.querySelector('.makeAvatar');

        // console.log(img.target);
        // console.log(btni);
        if (img.relatedTarget !== btn && img.relatedTarget !== btni) {
            const removeBtn = document.querySelector('.removeImg');
            hoveredImg.removeChild(removeBtn);
        }
        if (this.chngAva === true && img.relatedTarget !== btni) {
            hoveredImg.removeChild(btni);
        }

    }
    removeImg(evt, link) {
        this.sign.removeImg(link);
    }
    // animation methods
    showSingUp() {
        this.signup = !this.signup;
        if (this.signinState == 1) {
            this.signinState = this.signupState = this.stagerState = this.h3State = 0;
            this.email = this.signInEmail;
            this.password = this.signInPass;
        } else {
            this.signinState = 1;
            this.signInEmail = this.email;
            this.signInPass = this.password;
            this.password = this.confirmPassword = this.email = this.userName = this.userAvatar = '';
            this.age = null;
        }
    }
    showEditForm(v) {
        this.error = false;
        if (v === 1) {
            this.editNameState =  this.editNameState === 1 ? 0 : 1;
        } else {
            this.editAgeState =  this.editAgeState === 1 ? 0 : 1;
        }
    }
    editDone(e) {
        console.log(e);
    }
    done(ev) {
        if (ev.toState != 0) {
            this.signupState = 1;
        }
    }
    startSU(e) {
        if (e.toState != 0) {
            this.stagerState = 0;
        }
    }
    doneSU(e) {
        if (e.toState != 0) {
            this.stagerState = 1;
            this.h3State = 1;
        }
    }
}

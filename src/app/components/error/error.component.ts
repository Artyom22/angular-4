import { Component } from "@angular/core";

@Component({
    selector: "home",
    template: "<div class='notFound'>404 ERROR</div>",
    styles: [".notFound{width:100%; text-align:center; font-size:40px; padding:50px;}"]
})
export class ErrorComponent{}
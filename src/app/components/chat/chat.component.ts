import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { IterableDiffers } from '@angular/core';
import * as suService from '../../services/signup.service';


@Component({
    selector: 'chat',
    templateUrl: 'chat.component.html',
    styleUrls: ['chat.component.css']
})
export class ChatComponent  implements OnInit, OnDestroy {
    clickSendMessage;
    messages = [];
    connection;
    message;
    users;
    arrayOfKeys;
    logedUserId = this.sign.logedUserId;
    sendToUserId;
    arrayOfMessagesKeys;
    numArray = [];
    constructor(private chatService: ChatService, private sign: suService.SignupService) {
        this.chatService.getUsers().then(snapshot => {
            this.users = snapshot.val();
            console.log(this.users);
            this.arrayOfKeys = Object.keys(this.users);
            this.logedUserId = this.sign.logedUserId;
            const index = this.arrayOfKeys.indexOf(this.logedUserId);
            this.arrayOfKeys.splice(index, 1);
        });

    }

    sendMessage() {
        if (this.message !== undefined) {
            this.chatService.sendMessage(this.message, this.logedUserId, this.sendToUserId);

        }
        this.message = '';
    }

    clickMessage(sendToUserId) {
        const user = document.getElementById(sendToUserId);

        if (sendToUserId === this.sendToUserId) {
            this.clickSendMessage = !this.clickSendMessage;
        } else {
            const mess = document.querySelector('.newMessage');
            if (mess !== null) {
                user.removeChild(mess);
                this.numArray[sendToUserId] = 0;
            }
            this.clickSendMessage = true;
            this.sendToUserId = sendToUserId;
            this.logedUserId = this.sign.logedUserId;
            this.savedMessages();
        }
    }
    ngOnInit() {

        this.connection = this.chatService.getMessages().subscribe(mess => {
            // const message:{from, to, message} = mess;
             const message = Object.keys(mess);
             let from, to;
            for (const val of message) {
                if (val === 'from') {
                    from = mess[val];
                } else if (val === 'to') {
                    to = mess[val];
                }
            }
            if ((this.sendToUserId === from && this.logedUserId === to) || this.logedUserId === from) {
                this.messages.push(mess);
                const objDiv = document.querySelector('.messages');
                setTimeout(() => {
                    objDiv.scrollTop = objDiv.scrollHeight;
                }, 10);
            } else {
                const user = document.getElementById(from);
                if (from in this.numArray && this.numArray[from] !== 0) {
                    this.numArray[from] += 1;
                    console.log(this.numArray);
                    const messCount = document.querySelector('.newMessage');
                    messCount.innerHTML = `<i class="glyphicon glyphicon-envelope"></i> ` + this.numArray[from];
                } else {
                    this.numArray[from] = 1;
                    user.innerHTML += `
                    <div class="btn btn-danger newMessage">
                        <i class="glyphicon glyphicon-envelope"></i>
                         ` + this.numArray[from] + `</div>`;
                }
                console.log(this.numArray);
            }

        });
    }

/*    ngDoCheck() {
        let changes = this.iterableDiffer.diff(this.inputArray);
        if (changes) {
            console.log('Changes detected!');
        }
    }*/
    ngOnDestroy() {
        this.connection.unsubscribe();
    }

    savedMessages = () => {
        this.chatService.getSavedMessages(this.logedUserId, this.sendToUserId).then(snapshot => {
                // this.messages = snapshot.val();
                this.arrayOfMessagesKeys = Object.keys(snapshot.val());
                this.messages = [];
                let i = 0;
                this.arrayOfMessagesKeys.forEach(key => {
                    this.messages[i] = snapshot.val()[key];
                    i++;
                });
                const objDiv = document.querySelector('.messages');
                setTimeout(() => {
                    objDiv.scrollTop = objDiv.scrollHeight;
                }, 100);
                console.log(this.messages);
        }).catch(() => this.messages = []);
    }
/*    users;
    arrayOfKeys;
    logedUserId = this.sign.logedUserId;
    messeageShowHelper:number;
    constructor(private chat: ChatService, private sign: suService.SignupService) {
        this.chat.getUsers().then(snapshot => {
            this.users = snapshot.val();
            this.arrayOfKeys = Object.keys(this.users);
            const index = this.arrayOfKeys.indexOf(this.logedUserId);
            this.arrayOfKeys.splice(index, 1);
        });

    }
    clickMessage(i) {
        const tr = document.querySelector('.userRow' + i);
        const parentTr = document.querySelector('tbody');
        const otherMessageContents = document.querySelector('.sendMessageCont');
        if (otherMessageContents != null) {
            parentTr.removeChild(otherMessageContents);
        }
        if (this.messeageShowHelper !== i) {
            const newText = document.createElement('tr');
            newText.className = 'sendMessageCont';
            newText.innerHTML = `
                <td colspan="5" class="form-group ">
                    <div>

                    </div>
                    <textarea class="form-control" placeholder="Type message here"></textarea>
                    <button class="btn btn-success pull-right">Send</button>
                </td>`;
            tr.parentNode.insertBefore(newText, tr.nextSibling);
            this.messeageShowHelper = i;
        } else {
            this.messeageShowHelper = null;
        }
        // this.clickSendMessage = !this.clickSendMessage;
    }*/
}

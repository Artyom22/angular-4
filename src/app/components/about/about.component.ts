import {Component} from "@angular/core";
import * as suService from "../../services/signup.service";

@Component({
    selector: "home",
    template: `<h1>About us</h1>
                <div class="aboutPageHeader">You can see here our users. please click on <button class="btn btn-danger" (click)="showMoreCl()">this button</button></div>
                <div *ngIf="showMore">
                <users-form [aboutPage]="aboutPage"></users-form>
                </div>`,
    styles: [".aboutPageHeader{margin-bottom:30px;} "]
})
export class AboutComponent {
    showMore:boolean = false;
    aboutPage:boolean = false;

    showMoreCl(){
        this.showMore = !this.showMore;
    }
}
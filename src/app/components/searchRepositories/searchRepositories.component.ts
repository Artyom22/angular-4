import {Component} from "@angular/core";
import { Response} from '@angular/http';
import {SearchRepositoriesService} from "../../services/search-repositories.service";
import { trigger, state, style, transition, animate, keyframes, query, stagger} from "@angular/animations";
import {Subject} from 'rxjs/Subject';
// import * as suService from "../../services/signup.service";import * as suService from "../../services/signup.service";
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
@Component({
    selector:'searchRepositories',
    templateUrl: "searchRepositories.component.html",
    styleUrls: ["searchRepositories.component.css"],
    animations: [
        trigger("repositories",[
            state('0', style({transform:'translateX(50%) scale(0)', opacity:0})),
            state('1', style({transform:'translateX(0%) scale(1)', opacity:1})),
            ]
        ),
        trigger("repositoriesContent", [
            transition("* => *", [
                query(':enter', stagger('200ms', [
                    //style({transform:'translateX(50%) scale(0)', opacity:0,}),
                    animate('0.4s 0.2s ease', keyframes([
                        style({ transform: 'translateX(50%) scale(0)',  opacity:0,  offset: 0}),
                        style({ transform: 'translateX(-10%) scale(0)',  opacity:0.5, offset: 0.3}),
                        style({ transform: 'translateX(0%) scale(1)', opacity:1, offset: 1.0})
                    ]))
                ]), {optional: true})
            ])
        ])
    ]
})
export class SearchRepositoriesComponent {
    search:string;
    term$ = new Subject<string>();
    repositories: [{}];
    error:boolean = false;
    state:number = 0;
    repState:number = 0;
    page:number = 1;
    count:number = 5;
    totCount:number;

    constructor(private http:SearchRepositoriesService){
        this.term$
            .debounceTime(1000)
            .distinctUntilChanged()
            .subscribe(term => {this.searchRep(term)});
    }
    done(event){
        if(event.toState != 0){
            this.repState = 1;
        }
    }
    start(event){
        console.log();
        if(event.toState != 0){
            this.repState = 0;
        }    
    }
    searchRep(search){
        if (search != "") {
            this.http.getData(search, this.page, this.count).subscribe((data:Response) => {
                this.repositories = data.json().items;
                this.totCount = data.json().total_count;
                if(this.totCount >= 1000) {this.totCount = 1000;}
                this.error = false;
                this.state += 1;
            }
        );
        } else {
                this.error = true;
                this.state = 0;
        }
    }
    changePage(num){
        this.page = num;
        this.searchRep(this.search);
    }
}
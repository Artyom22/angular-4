/*
import { Component, Input } from '@angular/core';
import {trigger, transition, style, animate, query, stagger, state, keyframes} from '@angular/animations';
import {SearchRepositoriesService} from "../../services/search-repositories.service";
import {Subject} from "rxjs/Subject";
import {Response} from "@angular/http";

import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";


@Component({
    selector: 'git',
    templateUrl: 'searchRepositories.component.html',
    styles: [`
    .hidden{
    display: none;
    }
    `],
    animations: [
        trigger('testAnim', [
         state("0", style({
         opacity: 0
         })),
         state("1", style({
         opacity: 1
         }))
         ] ),
        trigger('listAnimation', [
            transition('* => *',  // each time the binding value changes
                query(':enter',  stagger("200ms", [
                        animate('0.5s', keyframes([
                            style({ opacity:0,  offset: 0}),
                            style({ opacity:0.5, offset: 0.3}),
                            style({  opacity:1, offset: 1.0})]))
                    ]),
                    { optional: true }),
                query(':leave',
                    stagger("100ms", [
                        animate('0.5s', style({ opacity: 0 }))
                    ]), { optional: true })
            )
        ])
    ],
    providers: [SearchRepositoriesService]
})
export class SearchRepositoriesComponent {
    title = 'Git';
    repositories=[];
    state:number = 0;
    temp$ = new Subject<string>();
    flag:number = 0;
    repositoriesCount = 0;
    @Input() open = false;

    constructor(private gitService: SearchRepositoriesService) {
        this.temp$.debounceTime(1000).distinctUntilChanged().subscribe(temp => this.searchRepository(temp));
    }
    searchRepository($event){
        this.flag +=1;
        if($event != "") {
            this.gitService.searchGitRepo($event)
                .subscribe( (result:Response) => {
                    this.repositories = result.json()['items'];
                    this.repositoriesCount = result.json()["total_count"];
                    this.flag +=1;
                }
                );
        } else {
            this.flag = 0;
        }
        // this.toggle();
        // console.log(this.state);
    }
    seeDescription($event){
        this.open = !this.open;
        $event.target.classList.toggle('fa-plus-circle');
        $event.target.classList.toggle('fa-minus-circle');
        document.getElementById($event.target.closest('td').classList.value).classList.toggle('hidden');
    }/!*
 showItems() {
 this.repositories ;
 }

 hideItems() {
 this.repositories = [];
 }

 toggle() {
 this.repositories.length ? this.hideItems() : this.showItems();
 }*!/
    animationStarted(event){
        if(event.toState != 0){
            this.state = 0
        }
    }
    animationDone(event){
     if(event.toState!=0){
     this.state = 1;
     }
    }
}
*/

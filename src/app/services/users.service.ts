export class Users {
    users;
    constructor(){
        this.users = [
            {name:"James", email:"asd@gmail.com", password:"sadasdasd", age:22},
            {name:"Walter", email:"vieo@gmail.com", password:"sadasdasd", age:50},
            {name:"Deen", email:"team@gmail.com", password:"sadasdasd", age:35}
        ];
    }


    getUsers() {
        return this.users;
    }
    addUserData(userName:string,  email:string, password:string, userAge:number) {
        this.users.push({name:userName, email:email, password:password, age:userAge});
    }
    deleteUser(i:number) {
        this.users.splice(i, 1);
    }
    edit(i:number, userName:string,  email:string, password:string, userAge:number) {
        this.users[i] = {name:userName, email:email, password:password, age:userAge};
    }

}
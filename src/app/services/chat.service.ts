import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

@Injectable()
export class ChatService {
    private url = 'http://localhost:5000';
    private socket;
    savedMessages;
    sendMessage(message, fromUser, toUser) {
        let user;
        if(fromUser > toUser) {
            user = fromUser + toUser;
        } else {
            user = toUser + fromUser;
        }
        console.log(user);

        console.log(message);
        let d = new Date();
        let date = d.getTime();
        firebase.database().ref('chat/' + user + '/' + date).set({
            message: message,
            from: fromUser,
            to : toUser
        });
        const messageData = {
            message: message,
            from: fromUser,
            to : toUser
        }
        this.socket.emit('add-message', messageData);
    }

    getMessages() {
        let observable = new Observable(observer => {
            this.socket = io(this.url);
            this.socket.on('message', (data) => {
                observer.next(data);
            });
            return () => {
                this.socket.disconnect();
            };
        })
        return observable;
    }
    getUsers() {
        return firebase.database().ref('/users/').once('value');
    }
    getSavedMessages(fromUser, toUser) {
        let user;
        if (fromUser > toUser) {
            user = fromUser + toUser;
        } else {
            user = toUser + fromUser;
        }
        return firebase.database().ref('chat/' + user).once('value');
    }
}
/*
import {Injectable} from '@angular/core';
import * as firebase from 'firebase/app';
import {config} from '../dataFiles/firebase';
import 'firebase/database';


@Injectable()
export class ChatService {

}
*/

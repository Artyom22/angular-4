import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class SearchRepositoriesService{

    constructor(private http: Http){ }


    getData(search, page, count){
        if(search != '') {
            return this.http.get('https://api.github.com/search/repositories?q='+search+'&sort=stars&order=desc&page='+page+'&per_page='+count+'');
        } else {
            return this.http.get('https://api.github.com/search/repositories');
        }
    }
}
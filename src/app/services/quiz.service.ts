import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import * as io from 'socket.io-client';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';
import { Subject } from 'rxjs/Subject';
import { SignupService} from './signup.service';

@Injectable()
export class QuizService {
    quiz$ = new Subject();
    private url = 'http://localhost:5000';
    socket = io(this.url);
    constructor(private sign: SignupService) {
    }

    getQuestions() {
        console.log(this.sign.logedUserId);
        firebase.database().ref('users/' + this.sign.logedUserId + '/num').once('value').then(snap => {
            const num = snap.val();
            firebase.database().ref('/quiz/question' + num).once('value').then(snapshot => {
                console.log(snapshot.val());
                this.addStreamData(snapshot.val());
                this.updateNum(num);
            }).catch(e => console.log(e));
        });
    }
    addStreamData(data) {
        this.quiz$.next(data);
    }
    getQuestionsLength() {
        return firebase.database().ref('/quiz/').once('value');
    }
    updateNum(num) {
        const updates = {};
        num += 1;
        const newNum = num;
        updates['users/' + this.sign.logedUserId + '/num'] = newNum;
        firebase.database().ref().update(updates).then(() => console.log('updated successfully'));
    }
    getTimeOut() {
/*        this.socket.emit('time');
        this.socket.on('timeO', (v) => {
            console.log(v);
            this.getQuestions(v);
        });*/

    }
    stop() {
/*        this.socket.emit('start');
        this.socket.on('timeA', (v) => {
            console.log(v);
            this.getQuestions(v);
        });*/

    }
}

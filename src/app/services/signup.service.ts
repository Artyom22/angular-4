import {Injectable} from '@angular/core';
import * as firebase from 'firebase/app';
import {config} from '../dataFiles/firebase';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';
import {Subject} from 'rxjs/Subject';

firebase.initializeApp(config);

export let logedUserID;
export let message;


@Injectable()
export class SignupService {
    logedUserId;
    userData;
    // dataObj = {};
    start = false;
    data$ = new Subject<{id: null, data: null, start: false, email: false}>(/*{id:null, data:null, start:false}*/);
    emailVerified = true;

    static verifyEmail() {
        firebase.auth().currentUser.sendEmailVerification().then(function() {
            message = 'We sent you verification email. Please check your email and verify your account to use it';
        }).catch(function(error) {
            message = 'Something went wrong. We can\'t sand you email, because we got this error -> ' + error;
        });
    }

    checkUser() {
        // this.start = true;
        return new Promise(function (resolve, reject) {
            firebase.auth().onAuthStateChanged(user => {
                if (user) {
                    // console.log(user.uid);
                    logedUserID = user.uid;
                    resolve([logedUserID, user.emailVerified]);
                } else {
                    logedUserID = null;
                    reject(logedUserID);
                }
            });
        });
    }

    getGoogleUser() {
        const provider = new firebase.auth.GoogleAuthProvider();
        provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
        return firebase.auth().signInWithPopup(provider);
    }

    addUser(name, email, age, password, img) {
        return new Promise(function(resolve, reject) {
            firebase.auth().createUserWithEmailAndPassword(email, password).then(v => {
                firebase.auth().onAuthStateChanged(user => {
                    if (user) {
                        logedUserID = user.uid;
                        firebase.database().ref('users/' + logedUserID).set({
                            username: name,
                            email: email,
                            age : age,
                            avatar: 'images%2F' + logedUserID + '%2F' + img.name
                        });
                        SignupService.verifyEmail();
                        // this.checkEmailVerification(user.emailVerified);
                        resolve(user.uid);
                    } else {
                        logedUserID = null;
                        reject(null);
                    }
                });
            }).catch(error => console.log(error));
        });
    }
    checkEmailVerification(verified){
        this.emailVerified = verified;
    }


    signIn(email, password) {
        return firebase.auth().signInWithEmailAndPassword(email, password);
    }

    signOutUser() {
        this.signOut().then(() => {
            this.logedUserId = null; this.addStreamData({id: null, data: null, start: true});
        }).catch(e => message = e);
    }

    takeUserData(id) {
        console.log(id);

        const usD = this.getUserData(id);
        if (usD != null) {
            console.log('asda 2564');
            this.logedUserId = logedUserID;
            usD.then(snapshot => {
                this.userData = {
                    name: snapshot.val().username,
                    age: snapshot.val().age,
                    email: snapshot.val().email,
                    id: logedUserID,
                    avatar: snapshot.val().avatar
                };
                this.addStreamData({id: this.logedUserId, data: this.userData, start: true, email:this.emailVerified});
            }).catch(e => console.log(e));
        }
        this.start = true;
    }

    updateName(name, uid) {
        const updates = {};
        updates['/users/' + uid + '/username'] = name;
        this.userData.name = name;
        this.addStreamData({id: this.logedUserId, data: this.userData, start: true});
        return firebase.database().ref().update(updates);
    }

    updateAge(age, uid) {
        const updates = {};
        updates['/users/' + uid + '/age'] = age;
        this.userData.age = age;
        this.addStreamData({id: this.logedUserId, data: this.userData, start: true});
        return firebase.database().ref().update(updates);
    }
    updatePass(pass) {
        const user = firebase.auth().currentUser;
        // let credential;
        return user.updatePassword(pass);
/*

        user.reauthenticateWithCredential(credential).then(function() {
            return "Your password updated successfully. Please sign in with your new password";
        }).catch(function(error) {
            console.log(error);
            return "Something went wrong. Please try again";
        });
*/


    }
    addStreamData(data) {
        if (data.id == null) {
            this.start = true;
        }
            this.data$.next(data);

        // this.data$.complete();
    }

    getUserData(id) {
        if (id != null) {
            logedUserID = id;
            return firebase.database().ref('/users/' + logedUserID).once('value');
        } else {
            logedUserID = null;
            firebase.auth().signOut().then(function() {
                logedUserID = null;
            }).catch(function(error) {
                message = 'Something went wrong. We got this error ' + error;
            });
            return null;
        }

    }

    addImage(img, rem=false) {
        const storage = firebase.storage();
        const storageRef = storage.ref();
        const imgName = img.name;
        const d = new Date();
        const date = d.getTime();
        const imagesRef = storageRef.child('images/' + this.logedUserId + '/' + date);
        imagesRef.put(img).then(function(snapshot) {
            console.log('Uploaded a blob or file!' + snapshot + imgName);
            firebase.database().ref('images/' + logedUserID + '/' + date).set({
                link: 'images%2F' + logedUserID + '%2F' + date,
                date: date
            });
        }).catch(e => console.log(e));
        if (rem === true) {
            return {date: date, link: 'images%2F' + logedUserID + '%2F' + date};
        }
    }
    updateAvatar(link) {
        const updates = {};
        updates['/users/' + this.logedUserId + '/avatar'] = link;
        this.userData.avatar = link;
        this.addStreamData({id: this.logedUserId, data: this.userData, start: true});
        return firebase.database().ref().update(updates);
    }
    getUserImages() {
        return firebase.database().ref('/images/' + this.logedUserId).once('value');
    }
    signOut() {
        return firebase.auth().signOut();
    }
    removeImg(img) {
        const newLink = img.link.replace(/%2F/g, '/');

        const storage = firebase.storage();
        const storageRef = storage.ref();
        const desertRef = storageRef.child(newLink);

        desertRef.delete().then(function() {
            console.log('removed from storage');
            console.log(img.date);

        }).catch(function(error) {
            // Uh-oh, an error occurred!
        });
        firebase.database().ref('images/' + this.logedUserId + '/' + img.date).set(null).then(() => {/*
                console.log(snapshot.val());
                console.log(snapshot);*/
                console.log('removed from database');
            }
        ).catch(() => console.log('something went wron'));
    }

/*
    */
}
